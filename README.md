# js-project2-as-jl
This project fetches a series of random quotes from the Ron Swanson API (https://ron-swanson-quotes.herokuapp.com/v2/quotes) and displays word by word at a speed that the user inputs. Once the user enters a number of step 50 between 50 and 1000, and press start, a word will appear with the center displayed in a different color. The user can stop the code, by pressing on the same button that they pushed to start. While the script is running or is in a paused state, the user can change the speed and this speed will be noticable once the user restarts the program. If the user enters an invalid value, it will round up or down to the nearest number which is a step of 50.

Bonus feature(s):

Title and the current speed display will change colors over time.
