"use strict";

/**
 * This application is a speed reader that functions by displaying individual words on the display on a speed set by the user. 
 * 
 * Angela Sposato 1934695
 * Jimmy Le 1936415
 * Fall 2020
 */

document.addEventListener("DOMContentLoaded", setup);

let para;
let before;
let focus;
let after;
let button;
let speed;
let totalspeed
let wpmSpeed;
let storedSpeed;
let isOn;
let runningInterval;
let index; 
let currentSpeed;
let btntimeout;
let isfinished;

let rainbowInterval;
let currentColor;
let colorSwitch;
let fullcolor;
/**
 * The setup function initializes values, calls the method that searches for wpmSpeed in localStorage
 * and adds event listeners for speed input field and start/stop button.
 * @author Angela
 */
function setup(){
    para = document.querySelector('#word');
    before = document.querySelector('#before');
    focus = document.querySelector('#focused');
    after = document.querySelector('#after');
    button = document.querySelector('#start');
    speed = document.querySelector('#speed');
    isOn = false;
    getLocalStorage();
    currentSpeed = wpmSpeed;
    displaySpeed();
    pulse();
    speed.addEventListener("input", speedChanger);
    button.addEventListener("click", startStop); 
}

/**
 * This method searches from localStorage to see if there is already a stored value for wpmSpeed. If not, will assign value to 100 and store in localStorage.
 * @author Angela
 */
function getLocalStorage(){
    wpmSpeed = JSON.parse(localStorage.getItem('wpmSpeed'));
    if (localStorage.getItem('wpmSpeed') === null){
        wpmSpeed = 100;
        localStorage.setItem('wpmSpeed', wpmSpeed);
        document.querySelector('#speed').value = wpmSpeed;
    }
    else{
        localStorage.setItem('wpmSpeed', wpmSpeed);
        speed.value = wpmSpeed;
    }
}
/**
 * Makes sure manually input values are not over 1000, under 50, and rounds other values to closest value of 50.
 * @author Angela
 */
function validateInputs(){
    if (wpmSpeed > 1000){
        wpmSpeed = 1000;
    }
    else if (wpmSpeed < 50){
        wpmSpeed = 50;
    }
    else if (wpmSpeed % 50 != 0){
        //Rounding up or down to next 50
        wpmSpeed = Math.round(wpmSpeed / 50) * 50;
    }
}

/**
 * Event handler for button. Acts depending on whether button is in start or stop mode.
 * @author Jimmy, Angela
 */
function startStop(){
    //if the button is on/started
    if(isOn){
        button.value = "Start";
        clearInterval(runningInterval);
        isOn = false;
        
    }
    //if the button is off
    else{
        button.value = "Stop";
        //using the wpmSpeed provided by the form/localstorage
        getNext();
        index = 0;
        currentSpeed = wpmSpeed;
        speed.value = wpmSpeed;
        displaySpeed();
        isfinished = false;
        isOn = true;
    }
}

/**
 * Fetches a quotes from API, then calls splitting method, then calls displayQuote with array of words from the quote
 * and catches any errors by calling treatError.
 * @author Angela
 * @param {event} e
 */
function getNext(e){
    let url = "https://ron-swanson-quotes.herokuapp.com/v2/quotes";
    fetch(url).then(response => {
        if (response.ok){
            return response.json();
        }
        else {
            throw new Error ('Status code: ' + response.status);
        }
    })
    .then (data => strSplitter(data))
    .then (arrayWords => displayQuote(arrayWords))
    .catch (error => treatError(error));
}
/**
 * This catches the error from getNext and displays it in the console. 
 * @author Angela
 * @param {error} error 
 */
function treatError(error){
    console.error(error);
}

/**
 * Takes as input a string
 * splits the string into an array
 * by using the space in between words as a separator
 * returns an array
 * @author Jimmy
 * @param {*} json 
 * @returns strarr
 */
function strSplitter(json){
    let strarr = json[0].split(" ");
    return strarr;
}

/**
 * Takes as input an array of words and displays each on the screen by passing the displayWord function to setInterval.
 * @author Jimmy
 * @param {} arr 
 */
function displayQuote(arr){
    if(!isfinished){
        clearInterval(runningInterval);
        totalspeed = (60000/currentSpeed);
        runningInterval = setInterval(displayWord, totalspeed, arr);
    }
    else{
        clearInterval(runningInterval);
        runningInterval = setInterval(displayWord, totalspeed, arr);
    }
}

/**
 * Takes an array of words as input. Each word is analyzed for the amount of letters it contains 
 * and portions of each word are put into spans created in the HTML file. 
 * After the last word from the array has been reached, getNext will be called to fetch another quote.  
 * @author Angela
 * @param {String[]} arrayWords
 */
index = 0;
function displayWord(arrayWords){
        console.log(arrayWords[index]);
        if (arrayWords[index].length == 1){
            before.textContent = '    ';
            focus.textContent = arrayWords[index];
            after.textContent = '';  
        }
        else if (arrayWords[index].length >= 2 && arrayWords[index].length <=5) {
            before.textContent = '   ' + arrayWords[index].substring(0,1);
            focus.textContent = arrayWords[index].substring(1,2);
            after.textContent = arrayWords[index].substring(2);
        }
        else if (arrayWords[index].length >=6 && arrayWords[index].length <= 9){
            before.textContent = '  ' + arrayWords[index].substring(0, 2);
            focus.textContent = arrayWords[index].substring(2, 3);
            after.textContent = arrayWords[index].substring(3);
        }
        else if (arrayWords[index].length >=10 && arrayWords[index].length <=13){
            before.textContent = ' ' + arrayWords[index].substring(0, 3);
            focus.textContent = arrayWords[index].substring(3, 4);
            after.textContent = arrayWords[index].substring(4);
        }
        else if (arrayWords[index].length > 13){
            before.textContent = arrayWords[index].substring(0, 4);
            focus.textContent = arrayWords[index].substring(4, 5);
            after.textContent = arrayWords[index].substring(5);
        }
        index++;
        if (index == arrayWords.length){
            isfinished = true;
            //Resetting the index to zero and calling getNext to fetch a new quote when quote finishes
            index = 0;
            getNext();
        }
    }

/**
 * when the input for the speed has been changed, 
 * check and convert the speed if the input is invalid
 * save speed to localstorage.
 * @author Jimmy
 * @param {*} e 
 */
function speedChanger(e){
    e.preventDefault;
    wpmSpeed = document.querySelector("#speed").value;
    validateInputs();
    localStorage.setItem("wpmSpeed", wpmSpeed);
}

/**
 * Displays the current speed of the interval running
 * allows the user to remember what the current speed is while they change through the inputs
 * @author Jimmy
 */
function displaySpeed(){
    let speedDisplay = document.querySelector("#currentspeed");
    speedDisplay.textContent = "Current Speed: " + currentSpeed + " wpm";
}


/**
 * Creates a new interval and initializes variables for the colors
 * @author Jimmy
 */
function pulse(){
    currentColor = 104;
    colorSwitch = false;
    rainbowInterval = setInterval(changeColors, 200);
}

/**
 * increments/decrements the currentColor variable and changes the color of the header text
 * @author Jimmy
 */
function changeColors(){
    //when colorSwitch is false ,change the background color and increments the currentColor
    if(!colorSwitch){
        fullcolor = "rgb(" + currentColor + ",104,247)";
        document.querySelector("h1").style.color = fullcolor;
        document.querySelector("#currentspeed").style.color = fullcolor;
        currentColor = currentColor + 1; 
    }
    //when colorSwitch is true, change the background color and decrement the currentColor
    else{
        fullcolor = "rgb(" + currentColor + ",104,247)";
        document.querySelector("h1").style.color = fullcolor;
        document.querySelector("#currentspeed").style.color = fullcolor;
        currentColor = currentColor - 1; 
    }
    //when the currentColor reaches 104, turn colorSwitch to false
    if(currentColor == 104){
        colorSwitch = false;
    }
    //when the currentColor reaches 247, turn colorSwitch to true
    else if(currentColor == 247){
        colorSwitch = true;
    }
}

